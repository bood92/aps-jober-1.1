@extends("layouts.data_table")
@section('plugins.html5-qrcode', true)

@section("control_card")
    <a class="btn btn-outline-primary" href="{{route("accounting.delivery.byQr")}}">Сканувати QR Код</a>
@endsection

@section("table_header")
    <tr>
        <th>Дата час</th>
        <th>Кількість</th>
        <th>Назва тоовару</th>
        <th>Категорыя товару</th>
        <th>Тип події</th>
        <th>Користувач</th>
        <th>Склад який передає</th>
        <th>Клієнт</th>
        <th>Працівник від клієнта</th>
    </tr>
@endsection

@section("table_body")
    @foreach($dataTable as $item)
        <tr>
            <td>{{$item->created_at}}</td>
            <td>{{$item->count}}</td>
            <td>{{$item->nomenclature}}</td>
            <td>{{$item->category}}</td>
            <td>{{$item->event_name}}</td>
            <td>{{$item->user_name}}</td>
            <td>{{$item->storage_name}}</td>
            <td>{{$item->client_name}}</td>
            <td>{{$item->client_worker_name}}</td>
        </tr>
    @endforeach
@endsection

@section("table_footer")
    <tr>
        <th>Дата час</th>
        <th>Кількість</th>
        <th>Назва тоовару</th>
        <th>Категорыя товару</th>
        <th>Тип події</th>
        <th>Користувач</th>
        <th>Склад який передає</th>
        <th>Клієнт</th>
        <th>Працівник від клієнта</th>
    </tr>
@endsection


@section("js")
    @parent

@endsection

@extends("layouts.page")
@section('plugins.jsQR', true)

@section("content")
    <div class="card card-default">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="device_id"></select>
                    </div>
                </div>
                <div class="col-6">
                    <button class="btn btn-outline-primary " id="scan">Сканувати</button>
                    <span id="qr_result"></span>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-6 offset-sm-3" id="outputContainer">
                    <video autoplay="" muted="" playsinline="" hidden="" id="videoContainer"></video>
                    <canvas id="canvas" hidden></canvas>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")
    @parent

    <script>
        let video = document.getElementById("videoContainer");
        let canvasElement = document.getElementById("canvas");
        let canvas = canvasElement.getContext("2d");
        let outputContainer = document.getElementById("outputContainer");

        let sel_device_id = $("#device_id");
        let btn_playVideo = $("#scan");
        let outputData = $("#qr_result");

        let tracks;

        getDevice();

        function getDevice() {
            navigator.mediaDevices.enumerateDevices()
                .then(function (devices) {
                    devices.forEach(function (device) {
                        console.log(device.kind + ": " + device.label +
                            " id = " + device.deviceId);

                        if (device.kind === "videoinput") {
                            let option = new Option(device.label, device.deviceId);
                            $("#device_id").append(option);

                        }

                    });
                })
                .catch(function (err) {
                    console.log(err.name + ": " + err.message);
                });
        }

        function playVideo(device_id) {
            const constraints = {
                audio: false,
                video: {
                    width: {min: 400 },
                    height: {min: 300 },
                    deviceId: device_id,
                    facingMode: "environment"
                }
            };
            navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
                video.srcObject = stream;
                tracks = stream.getTracks();
                video.play();
                requestAnimationFrame(tick);
            });
        }

        function stopStreaming() {
            tracks.forEach(function (track) {
                track.stop();
            });
            video.srcObject = null;
        }

        function drawLine(begin, end, color) {
            canvas.beginPath();
            canvas.moveTo(begin.x, begin.y);
            canvas.lineTo(end.x, end.y);
            canvas.lineWidth = 4;
            canvas.strokeStyle = color;
            canvas.stroke();
        }

        function tick() {
            if (video.readyState === video.HAVE_ENOUGH_DATA) {

                canvasElement.hidden = false;
                outputContainer.hidden = false;

                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                var code = jsQR(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                });
                if (code) {
                    drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
                    drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
                    drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
                    drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");

                    let data = JSON.parse(code.data);
                    if (data.hasOwnProperty("product_id")) {
                        location.href = `/accounting/delivery/by-product-id/${data.product_id}`;
                        stopStreaming();
                    }
                } else {
                    outputData.html("FALSE");
                }
            }
            requestAnimationFrame(tick);
        }

        btn_playVideo.click(function (e) {
            playVideo(sel_device_id.val());
            $("#selectCameraContainer").hide();
        });

    </script>

@endsection

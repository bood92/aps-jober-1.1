@extends("layouts.page")
@section("plugins.Select2", true)
@section("content")



    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Видача товару
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            {{Form::open(["method" => "POST", "url" => route("accounting.delivery.deliveryStore")])}}
                            {{Form::hidden("product_id", $productInfo->product_id)}}
                            {{Form::hidden("user_id", $userInfo->id)}}
                            {{Form::hidden("event_id", $event_id)}}
                            <div class="form-group">
                                {{Form::label( "Кількість" )}}
                                {{Form::number("count", "1", ["class" => "form-control", "id" => "count"])}}
                            </div>
                            <div class="form-group">
                                {{Form::label( "Клієнт якому передається" )}}
                                {{Form::select("client_id", $client_select, $productInfo->client_id, ["class" => "form-control select2", "id" => "client_id"])}}
                            </div>
                            <div class="form-group">
                                {{Form::label( "Працівник" )}}
                                {{Form::select("client_worker_id", ['' => __("form.base.attr.select_empty_val")] + $client_worker_select, null, ["class" => "form-control select2", "id" => "client_worker_id"])}}
                            </div>
                            {{Form::submit("Виконати", ["class" => "form-control btn btn-outline-success"])}}

                            {{Form::close()}}
                        </div>
                        <div class="col-8">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Користувач</th>
                                    <td>{{$userInfo->name}}</td>
                                </tr>
                                <tr>
                                    <th>Тип товару</th>
                                    <td>{{$productInfo->category}}</td>
                                </tr>
                                <tr>
                                    <th>Назва</th>
                                    <td>{{$productInfo->nomenclature}}</td>
                                </tr>
                                <tr>
                                    <th>Клієнт</th>
                                    <td>{{$productInfo->client_name}}</td>
                                </tr>
                                <tr>
                                    <th>Склад</th>
                                    <td>{{$productInfo->storage}}</td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    @parent
    <script>
        $(function () {
            $(".select2").select2();
        });

        let client_id = $("#client_id");
        let client_worker_id = $("#client_worker_id");

        client_id.on("change", onChangeClientId);
        function onChangeClientId() {
            $.get(`/accounting/delivery/getClientWorkerByClientId/${client_id.val()}`, (data) => {
                client_worker_id.html("");

                Object.keys(data).map((key) => {
                    let option = new Option(data[key], key);
                    client_worker_id.append(option);
                });

                client_worker_id.val(null);
            });
        }
    </script>
@endsection

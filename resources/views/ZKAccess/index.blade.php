@extends("layouts.simples.simple_form")
@section("plugins.daterangepicker", true)
@section("plugins.bootstrap4Duallistbox", true)

@section("js")
    @parent()

    <script>
        $(function () {
            $('.duallistbox').bootstrapDualListbox();


            let DatePickerLocaleOptions = scripts.getDatePickerLocaleOptions()
            DatePickerLocaleOptions.format = 'HH:mm:ss';

            let options = {
                timePicker: true,
                timePicker24Hour: true,
                timePickerIncrement: 1,
                timePickerSeconds: true,
                locale: DatePickerLocaleOptions
            };

            $("#timeRange").daterangepicker(options);
        });
    </script>

@endsection

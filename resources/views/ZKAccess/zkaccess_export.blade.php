<table>
    <thead>
    <tr>
        <td><strong> Дата час </strong></td>
        <td><strong> Прізвище </strong></td>
        <td><strong> Ім'я </strong></td>
        <td><strong> Пристрій </strong></td>
    </tr>
    </thead>
    <tbody>
    @foreach($dataTable as $item)
        <tr>
            <td width="40">{!! $item[0] !!}</td>
            <td width="40">{!! $item[2] !!}</td>
            <td width="40">{!! $item[3] !!}</td>
            <td width="40">{!! $item[5] !!}</td>
        </tr>
    @endforeach
</table>

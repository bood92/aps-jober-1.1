<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

return [
    "users" => [
        "avatar" => [
            "path" => "public/users/avatars",
            "default" => "public/users/avatars/default.png"
        ]
    ],
    /*Знаків після коми*/
    "round" => [
        "db" => 2,
        "public" => 2
    ],

    "monthly_services_id" => [24,25,26,27,34,35,37],

    "ui_setting" => [
        "pagination" => 50
    ],

    "ZKAccess" => [
        "controllers" => [
            "КПП_1(Офіс)"       => "КПП_1(Офіс)",
            "Кпп_1(Торговий)"   => "Кпп_1(Торговий)",
            "КПП_1_вихід-вхід"  => "КПП_1_вихід-вхід",
            "КПП_1_Вхід-вихід"  => "КПП_1_Вхід-вихід",
            "КПП_3 Бойня курей" => "КПП_3 Бойня курей",
            "КПП-1 Настасів"    => "КПП-1 Настасів",
            "Пост_6(1)"         => "Пост_6(1)",
            "Пост_6(2)"         => "Пост_6(2)",
        ]
    ],
];

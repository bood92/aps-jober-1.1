<?php


namespace App\Exports\ZKAccess;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ZKAccessExport implements FromView, WithTitle
{
    private $data = [];
    private $title = "Title";

    public function __construct(array $data, string $title)
    {
        $this->data = $data;
        $this->title = $title;
    }

    public function view(): View
    {
        return view("ZKAccess.zkaccess_export", [
            "dataTable" => $this->data,
        ]);
    }

    public function title(): string
    {
        return  $this->title;
    }

}

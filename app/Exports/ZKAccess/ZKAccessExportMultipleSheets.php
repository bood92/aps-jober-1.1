<?php


namespace App\Exports\ZKAccess;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZKAccessExportMultipleSheets  implements  WithMultipleSheets
{
    use Exportable;

    public $sheets = array();

    public function __construct(array $sheets)
    {
        $this->sheets = $sheets;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->sheets() as $key => $sheet) {
            $sheets[] = new ZKAccessExport($sheet, $key);
        }

        return  $sheets;
    }
}

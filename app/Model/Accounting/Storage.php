<?php

namespace App\Model\Accounting;

use App\Model\Clients;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Storage
 * @package App\Model\Accounting
 */
class Storage extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_storage";

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "client_id"
    ];

    /**
     * @return HasOne
     */
    public function client()
    {
        return $this->hasOne(Clients::class, "id", "client_id");
    }

    /**
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Products::class, "storage_id", "id");
    }

}

<?php

namespace App\Model\Accounting;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Products
 * @package App\Model\Accounting
 */
class Products extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_products";

    /**
     * @var array
     */
    protected $fillable = [
        "nomenclature_id",
        "storage_id",
        "user_id",
        "remainder",
        "model",
        "serial_number",
        "inventory_number",
    ];

    /**
     * @return HasOne
     */
    public function nomenclature()
    {
        return $this->hasOne(Nomenclature::class, "id", "nomenclature_id");
    }

    /**
     * @return HasOne
     */
    public function storage()
    {
        return $this->hasOne(Storage::class, "id", "storage_id");
    }

    /**
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * @return HasMany
     */
    public function delivery()
    {
        return $this->hasMany(Delivery::class, "product_id", "id");
    }

    public function deliverProduct($product_id) {



    }

}

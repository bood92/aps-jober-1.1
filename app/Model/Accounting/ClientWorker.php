<?php

namespace App\Model\Accounting;

use App\Model\Clients;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ClientWorker
 * @package App\Model\Accounting
 */
class ClientWorker extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_client_worker";

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "description",
        "client_id",
    ];

    /**
     * @return HasOne
     */
    public function client()
    {
        return $this->hasOne(Clients::class, "id", "client_id");
    }

    public function delivery() {
        return $this->hasMany(Delivery::class, "client_worker_id", "id");
    }

}

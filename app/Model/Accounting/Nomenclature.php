<?php

namespace App\Model\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Nomenclature
 * @package App\Model\Accounting
 */
class Nomenclature extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_nomenclature";

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "category_id"
    ];

    /**
     * @return HasOne
     */
    public function category_nomenclature()
    {
        return $this->hasOne(CategoriesNomenclature::class, "id", "category_id");
    }

    /**
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Products::class, "nomenclature_id", "id");
    }

}

<?php

namespace App\Model\Accounting;

use App\Model\Units;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CategoriesNomenclature
 * @package App\Model\Accounting
 */
class CategoriesNomenclature extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_categories_nomenclature";

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "unit_id"
    ];

    /**
     * @return HasOne
     */
    public function unit()
    {
        return $this->hasOne(Units::class, "id", "unit_id");
    }

    /**
     * @return HasMany
     */
    public function nomenclature()
    {
        return $this->hasMany(Nomenclature::class, "category_id", "id");
    }

}

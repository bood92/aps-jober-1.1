<?php

namespace App\Model\Accounting;

use App\Helpers\ModelHelper;
use App\Model\Clients;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Delivery
 * @package App\Model\Accounting
 */
class Delivery extends Model
{
    use ModelHelper;

    /**
     * @var string
     */
    protected $table = "acc_delivery";

    /**
     * @var array
     */
    protected $fillable = [
        "count",
        "product_id",
        "event_id",
        "user_id",
        "client_worker_id",
        "client_id",
    ];

    /**
     * @return HasOne
     */
    public function product()
    {
        return $this->hasOne(Products::class, "id", "product_id");
    }

    /**
     * @return HasOne
     */
    public function event()
    {
        return $this->hasOne(EventsDelivery::class, "id", "event_id");
    }

    /**
     * @return HasOne
     */
    public function client_worker()
    {
        return $this->hasOne(ClientWorker::class, "id", "client_worker_id");
    }

    /**
     * @return HasOne
     */
    public function client()
    {
        return $this->hasOne(Clients::class, "id", "client_id");
    }

    /**
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function store(array $data, $id = null) {


        $model = clone  $this;

        if ($id != null) {
            $model = $this->where("id", $id)->first();
        }
        $model->fill($data);
        $storeDelivery =  $model->save();

        if ($storeDelivery) {
            $ProductModel = Products::whereId($data["product_id"])->first();
            $ProductModel->remainder = $ProductModel->remainder - $data["count"];
            return $ProductModel->save();
        }else {
            return false;
        }
    }


}

<?php

namespace App\Model\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class EventsDelivery
 * @package App\Model\Accounting
 */
class EventsDelivery extends Model
{
    /**
     * @var string
     */
    protected $table = "acc_events_delivery";

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "description"
    ];

    /**
     * @return HasMany
     */
    public function delivery()
    {
        return $this->hasMany(Delivery::class, "event_id", "id");
    }

}

<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\ClientWorker as Model;
use App\Repository\CoreRepository;

/**
 * Class ClientWorkerRepository
 * @package App\Repository\Accounting
 */
class ClientWorkerRepository extends CoreRepository
{
    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $client_id
     * @return mixed
     */
    public function getForSelectByClient(int $client_id)
    {
        $result = $this->startCondition()
            ->orderBy("name", "ASC")
            ->where("client_id", $client_id)
            ->get()
            ->pluck("name", "id")
            ->toArray();

        return $result;
    }

}

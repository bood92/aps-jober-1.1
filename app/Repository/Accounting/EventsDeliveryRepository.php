<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\EventsDelivery;
use App\Model\Accounting\EventsDelivery as Model;
use App\Repository\CoreRepository;

/**
 * Class EventsDeliveryRepository
 * @package App\Repository\Accounting
 */
class EventsDeliveryRepository extends CoreRepository
{
    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @return mixed
     */
    public static function getDeliveryEventId()
    {
        return EventsDelivery::where("name", "delivery")->first()->id;
    }
}


<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\Nomenclature as Model;
use App\Repository\CoreRepository;

class NomenclatureRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }
}

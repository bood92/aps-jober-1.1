<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\Products as Model;
use App\Repository\CoreRepository;
use Illuminate\Support\Facades\DB;


/**
 * Class ProductsRepository
 * @package App\Repository\Accounting
 */
class ProductsRepository extends CoreRepository
{
    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getAllInfoByProductId($product_id)
    {
        $columns = [
            DB::raw("acc_products.id as product_id"),
            DB::raw("acc_nomenclature.name as nomenclature"),
            DB::raw("acc_categories_nomenclature.name as 'category'"),
            DB::raw("acc_storage.name as 'storage'"),
            DB::raw("clients.name as 'client_name'"),
            DB::raw("clients.id as 'client_id'"),
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->where("acc_products.id", $product_id)
            ->join("acc_nomenclature", "acc_nomenclature.id", "acc_products.nomenclature_id")
            ->join("acc_categories_nomenclature", "acc_categories_nomenclature.id", "acc_nomenclature.category_id")
            ->join("acc_storage", "acc_storage.id", "acc_products.storage_id")
            ->join("clients", "acc_storage.client_id", "clients.id")
            ->toBase()
            ->first();

        return $result;
    }

}

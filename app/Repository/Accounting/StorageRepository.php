<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\Storage as Model;
use App\Repository\CoreRepository;

class StorageRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }
}

<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\Delivery as Model;
use App\Repository\CoreRepository;
use Illuminate\Support\Facades\DB;

class DeliveryRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getForDataTable() {
        $columns = [
            DB::raw("acc_delivery.created_at"),
            DB::raw("acc_delivery.count"),
            DB::raw("acc_nomenclature.name as 'nomenclature'"),
            DB::raw("acc_categories_nomenclature.name as 'category'"),
            DB::raw("acc_events_delivery.name as 'event_name'"),
            DB::raw("acc_events_delivery.description as 'event_description'"),
            DB::raw("users.name as 'user_name'"),
            DB::raw("acc_storage.name as 'storage_name'"),
            DB::raw("clients.name as 'client_name'"),
            DB::raw("acc_client_worker.name as 'client_worker_name'"),
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->join("acc_products", "acc_products.id", "acc_delivery.product_id")
            ->join("acc_nomenclature", "acc_nomenclature.id", "acc_products.nomenclature_id")
            ->join("acc_categories_nomenclature", "acc_categories_nomenclature.id", "acc_nomenclature.category_id")
            ->join("acc_events_delivery", "acc_events_delivery.id", "acc_delivery.event_id")
            ->join("users", "users.id", "acc_delivery.user_id")
            ->join("acc_storage", "acc_storage.id", "acc_products.storage_id")
            ->join("clients", "clients.id", "acc_delivery.client_id")
            ->leftJoin("acc_client_worker", "acc_delivery.client_worker_id", "acc_client_worker.id")
            ->get();

        return $result;

    }

}

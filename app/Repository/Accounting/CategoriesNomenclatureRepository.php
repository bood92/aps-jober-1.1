<?php

namespace App\Repository\Accounting;

use App\Model\Accounting\CategoriesNomenclature as Model;
use App\Repository\CoreRepository;

class CategoriesNomenclatureRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }
}

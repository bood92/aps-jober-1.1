<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ZKAccessForm extends Form
{
    public function buildForm()
    {

        $this
            ->add("xlsx_file", "file" ,[
                "label" => "XLSX File",
                "attr" => [
                    "accept" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                ]
            ])
            ->add("except_controller", "select",  [
                "label" => "Виключити контроллери",
                'choices' => config("my.ZKAccess.controllers"),
                'attr' => [
                    "class" => "duallistbox",
                    'expanded' => true,
                    "multiple" => "multiple",
                    "id" => "except_controller",
                    "style" => "width: 100%",
                ]
            ])
            ->add("timeRange", "text", [
                "label" => "Час",
                "attr" => [
                    "id" => "timeRange"
                ]
            ])
            ->add("submit", "submit", [
                "label" => "Завантажити",
                "attr" => [
                    "class" => "form-control btn-success",
                ]
            ]);

    }
}

<?php


namespace App\Services;


use Carbon\Carbon;
use Generator;
use Illuminate\Http\UploadedFile;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ZKAccess
{
    /**
     * @var UploadedFile
     */
    private $uploadedFile;

    private $timeRange;

    private $exceptController;


    public function getReport(UploadedFile $uploadedFile, string $timeRange, $exceptController = null)
    {
        $this->uploadedFile = $uploadedFile;
        $this->setTimeRange($timeRange);
        $this->exceptController = (is_array($exceptController)) ? $exceptController : [];

        $result = $this->prepareReportData();
        return $result;
    }

    public function prepareReportData() {
        $result = [];
        foreach ($this->getRowGenerator() as $item) {

            if ($this->filter_exceptController($item[5])) continue;


            $dateTime = explode(" ", $item['0']);
            $filterTimeRangeRange = Carbon::createFromDate($dateTime[1])->between($this->timeRange["start"], $this->timeRange["stop"]);
            if ($filterTimeRangeRange) {
                if (isset($result[$dateTime[0]][$item[1]]) AND $result[$dateTime[0]][$item[1]][0] < $item[0]) {
                    $result[$dateTime[0]][$item[1]] = $item;
                } else {
                    $result[$dateTime[0]][$item[1]] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $timeRange
     */
    public function setTimeRange(string $timeRange) : void {
        $timeRange = explode(" - ", $timeRange);
        $this->timeRange = [
            "start" => $timeRange[0],
            "stop" => $timeRange[1]
        ];
    }

    /**
     * @param string $controller
     * @return bool
     */
    public function filter_exceptController(string $controller) : bool {
        return (in_array($controller, $this->exceptController));
    }

    /**
     * @return Generator
     */
    private function getRowGenerator($startIteration = 1)
    {
        $IOFactory = IOFactory::load($this->uploadedFile->path());
        $array = $IOFactory->getActiveSheet()->toArray();
        for ($i = $startIteration; $i < count($array); $i++) {
            yield($array[$i]);
        }
    }

    /**
     * @param array $data
     * @param string $filePath
     * @param string $fileName
     * @return bool
     */
    public function storeMultipleSheet(array $data, string $filePath, string $fileName) {
        $spreadsheet = new Spreadsheet();

        foreach ($data as $key => $item) {
            if (array_key_first($data) === $key) {
                $sheet = $spreadsheet->getActiveSheet();
            }else {
                $sheet = $spreadsheet->createSheet();
            }

            $sheet->setTitle($key);
            $sheet->fromArray($item);
        }

        $writer = new Xlsx($spreadsheet);

        try{
            $writer->save($filePath.$fileName);
        }catch (\Exception $e) {
            return false;
        }

        return true;
    }

}

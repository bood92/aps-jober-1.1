<?php

namespace App\Http\Controllers\Accounting;

use App\Helpers\ControllerHelper;
use App\Http\Controllers\Controller;
use App\Model\Accounting\Delivery;
use App\Repository\Accounting\ClientWorkerRepository;
use App\Repository\Accounting\DeliveryRepository;
use App\Repository\Accounting\EventsDeliveryRepository;
use App\Repository\Accounting\ProductsRepository;
use App\Repository\ClientRepository;
use App\Repository\UsersRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Class DeliveryController
 * @package App\Http\Controllers\Accounting
 */
class DeliveryController extends Controller
{
    use ControllerHelper;

    /**
     * @var ProductsRepository
     */
    private $ACC_ProductsRepository;

    /**
     * @var ClientWorkerRepository
     */
    private $ACC_ClientWorkerRepository;
    /**
     * @var DeliveryRepository
     */
    private $ACC_DeliveryRepository;

    /**
     * @var ClientRepository
     */
    private $ClientsRepository;

    /**
     * @var UsersRepository
     */
    private $UsersRepository;

    /**
     * DeliveryController constructor.
     */
    public function __construct()
    {
        $this->ACC_ProductsRepository = app(ProductsRepository::class);
        $this->ACC_ClientWorkerRepository = app(ClientWorkerRepository::class);
        $this->ACC_DeliveryRepository = app(DeliveryRepository::class);
        $this->UsersRepository = app(UsersRepository::class);
        $this->ClientsRepository = app(ClientRepository::class);
    }

    /**
     * @return Factory|View
     */
    public function index()
    {

        $dataTable = $this->ACC_DeliveryRepository->getForDataTable();

        return view("accounting.delivery.index", [
            "title" => "Облік картриджів",
            "card_title" => "Облік картриджів",
            "dataTable" => $dataTable,
        ]);
    }

    /**
     * @return Factory|View
     */
    public function byQr()
    {
        return view("accounting.delivery.scan_qr_code", [
            "title" => "Видача картриджів",
        ]);
    }

    /**
     * @param $productId
     * @return Factory|View
     */
    public function byProductId($productId)
    {
        $productInfo = $this->ACC_ProductsRepository->getAllInfoByProductId($productId);

        return view("accounting.delivery.by_product_id", [
            "title" => "Видача товару",
            "productInfo" => $productInfo,
            "userInfo" => $this->UsersRepository->getForEdit(Auth::id()),
            "event_id" => EventsDeliveryRepository::getDeliveryEventId(),
            "client_select" => $this->ClientsRepository->getForSelect(),
            "client_worker_select" => $this->ACC_ClientWorkerRepository->getForSelectByClient($productInfo->client_id)

        ]);
    }

    /**
     * @param $client_id
     * @return JsonResponse
     */
    public function getClientWorkerByClientId($client_id)
    {
        $result = $this->ACC_ClientWorkerRepository->getForSelectByClient($client_id);
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function deliveryStore(Request $request)
    {
        Validator::make($request->input(), [
            "product_id" => "required",
            "user_id" => "required",
            "event_id" => "required",
            "count" => "required",
            "client_id" => "required",
            "client_worker_id" => "",
        ])->validate();


        $DeliveryModel = new Delivery();
        $result = $DeliveryModel->store($request->except("_token"));

        return ($result)
            ? redirect()->route("accounting.delivery.index")->with("message", __("basic.messages.success"))
            : redirect()->route("accounting.delivery.byQr")->withErrors(__("basic.messages.error"));
    }


}

<?php

namespace App\Http\Controllers\ZKAccess;

use App\Exports\ZKAccess\ZKAccessExport;
use App\Exports\ZKAccess\ZKAccessExportMultipleSheets;
use App\Forms\ZKAccessForm;
use App\Helpers\ControllerHelper;
use App\Http\Controllers\Controller;
use App\Imports\ZKAccessImport;
use App\Services\ZKAccess;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Kris\LaravelFormBuilder\FormBuilder;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ZKAccessController extends Controller
{
    use ControllerHelper;

    /**
     * @var ZKAccess
     */
    private $ZKAccessService;

    public function __construct()
    {
        $this->ZKAccessService = new ZKAccess();
    }

    public function index(FormBuilder $formBuilder)
    {

        $form = $formBuilder->create(ZKAccessForm::class, [
            "method" => "POST",
            "url" => route("zkaccess.getReport"),
        ]);

        return view("ZKAccess.index", [
            "form" => $form,
            "title" => "ZKAccess reports",
            "card_title" => "ZKAccess reports",
        ]);
    }

    public function getReport(Request $request)
    {
        Validator::make($request->all() , [
            "xlsx_file" => "required|mimes:xls,xlsx",
            "timeRange" => "required",
            "except_controller" => "",
        ])->validate();



        $result = $this->ZKAccessService->getReport(
            $request->file("xlsx_file"),
            $request->timeRange,
            $request->except_controller
        );

        $filePath = Storage::disk('local')->getAdapter()->getPathPrefix()."tmp/";
        $fileName = "ZKAccessReportTmp.xlsx";

        if ($this->ZKAccessService->storeMultipleSheet($result, $filePath, $fileName)) {
            return response()->download(storage_path("app/tmp/{$fileName}"));
        }else {
            return $this->resultRedirect(false);
        }
    }

    public function  getMem () {
        return ['Usage: ' . (memory_get_usage(true) / 1024 / 1024) . ' MB', 'Peak: ' . (memory_get_peak_usage(true) / 1024 / 1024) . ' MB'];
    }
}

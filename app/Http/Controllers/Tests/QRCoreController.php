<?php

namespace App\Http\Controllers\Tests;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QRCoreController extends Controller
{
    public function qr_code () {
        return view("tests/qr_code");
    }
}

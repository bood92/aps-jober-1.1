<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddClientWorkerIdAccDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_delivery', function (Blueprint $table) {
            $table->unsignedBigInteger("client_worker_id")->nullable();
            $table->foreign("client_worker_id") -> references("id") -> on("acc_client_worker");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_delivery', function (Blueprint $table) {
            $table->dropColumn("client_worker_id");
        });
    }
}

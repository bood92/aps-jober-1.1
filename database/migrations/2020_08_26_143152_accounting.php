<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Accounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Категорії товарів
         */
        Schema::create('acc_categories_nomenclature', function (Blueprint $table) {
            $table->id();
            $table->string("name");

            $table->unsignedBigInteger("unit_id");
            $table->foreign("unit_id")->references("id")->on("units");

            $table->timestamps();
        });

        /**
         * Склад
         */
        Schema::create('acc_storage', function (Blueprint $table) {
            $table->id();
            $table->string("name");

            $table->unsignedBigInteger("client_id");
            $table->foreign("client_id") ->references("id") -> on("clients");

            $table->timestamps();
        });

        /**
         * Номенклатура товарів
         */
        Schema::create('acc_nomenclature', function (Blueprint $table) {
            $table->id();
            $table->string("name");

            $table->unsignedBigInteger("category_id");
            $table->foreign("category_id") -> references("id")->on("acc_categories_nomenclature");

            $table->timestamps();
        });

        /**
         * Товари
         */
        Schema::create('acc_products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger("nomenclature_id");
            $table->foreign("nomenclature_id") -> references("id")->on("acc_nomenclature");

            $table->unsignedBigInteger("storage_id");
            $table->foreign("storage_id") -> references("id")->on("acc_storage");

            $table->integer("remainder")->default(0);
            $table->string("model") -> nullable();
            $table->string("serial_number")->nullable();
            $table->string("inventory_number")->nullable();
        });


        /**
         * Події
         */
        Schema::create('acc_events_delivery', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->timestamps();
        });

        /**
         * Облік
         */
        Schema::create('acc_delivery', function (Blueprint $table) {
            $table->id();

            $table->integer("count")->default(1);

            $table->unsignedBigInteger("product_id");
            $table->foreign("product_id") -> references("id")->on("acc_products");

            $table->unsignedBigInteger("event_id");
            $table->foreign("event_id") -> references("id")->on("acc_events_delivery");

            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id") -> references("id")->on("users");

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_categories_nomenclature');
        Schema::dropIfExists('acc_storage');
        Schema::dropIfExists('acc_nomenclature');
        Schema::dropIfExists('acc_products');
        Schema::dropIfExists('acc_events_delivery');
        Schema::dropIfExists('acc_delivery');
    }
}

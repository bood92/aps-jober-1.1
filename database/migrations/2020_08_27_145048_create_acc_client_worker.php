<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccClientWorker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_client_worker', function (Blueprint $table) {
            $table->id();
            $table->string("name");

            $table->unsignedBigInteger("client_id");
            $table->foreign("client_id")->references("id")->on("clients");
            $table->string("description") -> nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_client_worker');
    }
}
